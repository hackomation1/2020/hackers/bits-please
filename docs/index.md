![SPRAI LOGO](img/Sprai.png "Sprai")
## Description
#### Sprai is a modular contactless automatic temperature scanning and hand sanitizing system. With this system companies can offload temperature scanning and sanitizing work from their employees and let our automatic solution handle that for them with a nice dashboard that'll give all sorts of alerts and logs of everything that's been going on or has happened.

## Overview & Features
#### Features
- #### Automatic temperature scanning
- #### Automatic hand sanitizer sprayer
- #### Alerts
    - **Abnormal tempatures** 
    - **Almost out of sanitizer fluid**

- #### Dashboard: 
    - **Overview of scanned tempatures**
    - **Most recent highest tempatures**
    - **Most recent lowest tempatures**
    - **More filters for data processing up to your needs**

## Demo / Proof of work
![SPRAI DEMO](img/demo.mp4 "Demo")
![SPRAI prod](img/final prod.jpeg "product")
![SPRAI spray](img/spray side view.jpeg "side view")
![SPRAI prod](img/dashboard.jpeg "dashboard")

## Deliverables
- Proof of concept working (check video above)
- Product pitch deck **[[Link to pitch](https://docs.google.com/presentation/d/1fA2cVz0NwE9dltK7RJjLm33oRQV6m2wv9nQTzxzy8EI/edit?usp=sharing)]**

## the team & contact
- **Andojo Mack** (andojomack@gmail.com)
- **Joel Naarendorp** (joelhammen11@gmail.com)
- **Shaniel Samadhan** (shaniel29samadhan@gmail.com)
- **Lesley Uiterloo** (braston15@gmail.com)
