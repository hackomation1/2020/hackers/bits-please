# 4. Business & Marketing
![Sprai](docs/img/Sprai.png)

## 4.1 Objectives

### Background
#### Currently there needs to be a person in the area to check the temperature of a client and to give them hand sanitizer. The problem with  is that you increase the chances of getting your employees and customers infected with diseases and that people aren't always trustworthy.

### Solution
#### With our system you can scan and sanitize the client's hands without any physical contact. The data that is stored in each individual system can be collected and sold to health institutions or the government so that they can do all sorts of data analytics to check areas of high temperatures for example. Your employee can go do a more meaningful job too instead of sanitizing people all day long.
 

## 4.3 Ananlyze your market & segments

### Customer Segments 
- **Super markets**
- **Shopping Malls**
- **Airports**
- **Hospitals**
- **Small to Large business**

### Value Proposition

- **Modular design** (Make the product customizable for each individual customer || Ability to plug it into existing systems)
- **Risk Reduction** (This product will be used without clients having to make physical contact with it)
- **Employees can be put to do more meaningful work**

### Channels
- **Digital Marketing**
- **Direct Marketing**
- **Word of Mouth**
- **Door to Door Marketing**
- **Wholesale Retailers**
- **TV / Radio Channels**

## 4.4 Build your business canvas
**[[Link to business Canvas](https://canvanizer.com/canvas/rJnwYyvVSufZb)]**
![Canvas](docs/img/canvas.png)

## 4.5 Build your pitch deck
**[[Link to Project Pitch](https://docs.google.com/presentation/d/1fA2cVz0NwE9dltK7RJjLm33oRQV6m2wv9nQTzxzy8EI/edit?usp=sharing)]**

## 4.6 Make a project Poster
![POSTER](docs/img/poster.png)

## 4.7 Files & Code
**[[Link to code](files/code.zip)]**

## 4.8 References & Credits
**Bits-Please**