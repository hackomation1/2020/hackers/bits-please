# 5. Blog 

### ==[`11 nov 2020`]==

# Building the schematic for the temperature scanner and distance sensor

We went to work on building the temperature scanning mechanism. After that we started experimenting with the distance sensor to detect the motion
when the user is gonna put their hands to get sprayed.

![nov 11](img/nov 11.mp4)


### ==[`21 nov 2020`]==

# Pitch recording and 3D printing parts

We had to come to the lab on this day to record our pitch for the project. We came and Shaniel did the presentation. Furthermore we started
printing the parts that we needed for the spraying mechanism.

![nov 21](img/nov 21.mp4)
![nov 21_1](img/nov 21_1.mp4)


### ==[`28 nov 2020`]==

# Putting together our project

On this day, we were basicaly the whole day in the lab. We put together our project and tested it thoroughly to make sure it was presentation ready.

![spray front](img/spray front view.jpeg)
![final project](img/WhatsApp Image 2020-11-29 at 15.49.40.jpeg)


### ==[`29 nov 2020`]==

# Pitch day!

We were ready to pitch. We went to the lab to do some final touch ups to the project. And then hell rained down. Suddenly nothing worked anymore. Like 
nothing at all! We were pannicking and debugging it. And then it was time to go to Lalla Rookh. We decided to just go without a working product. The pitch actually went well. There were alot of interesting questions that we got too. All in all, it was a day worth remembering!

![Pitch](img/pitch day.jpeg)
