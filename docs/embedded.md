# 2. Embedded solution

## 2.1 Objectives
### Problem
#### To build such a versatile system we need components which work well together. And since this is an itegrated system that should have IoT in it, we needed a microcontroller that had networking capabillities. We also needed other sensors to detect motion and temperature and a motor to drive the spraying mechanism

### Solution 
#### We chose to use the TTGO ESP32 module which had all of the components we needed. It has wifi, bluetooth and even an OLED screen. Furthermore we decided to use a non contact temperature sensor, an infrared distance sensor for motion detection and a servo to drive the spraying mechanism. We put the parts together, programmed all the functionality. And it worked! We now have a system that can automatically scan your body temperature, and then spray hand sanitizer.


## 2.3 Steps taken
- **Buy all the nescesarry parts**
- **Test all the parts**
- **Wire everything up on a breadboard**
- **Write the code**
- **Test the system**
- **Solder the circuit**
- **Put the system in an enclosure**

## 2.4 Testing & Problems
#### We encountered alot of technical problems along the way. The main one was that the 3D printed parts for the spraying mechanism was not correct and did not work. We then decided to modify some parts and redo everything. Then we managed to get the spraying mechanism working.

## 2.5 Proof of work
![SPRAI DEMO](img/demo.mp4 "Demo")
![SPRAI spray](img/schematic.jpeg "schem")
![SPRAI spray](img/nov 11.mp4 "schem view")
![SPRAI prod](img/final prod.jpeg "product")

## 2.6 Files & Code
**[[Link to code](files/code.zip)]**
**[[Link to STL files](files/sprayer mechanism design.zip)]**

## 2.7 References & Credits
**Bits-Please**