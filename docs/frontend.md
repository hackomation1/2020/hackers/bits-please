# 3. Front-End & Platform

## 3.1 Objectives
### Problem
#### To build such a versatile system we had to make a dashboard which was capable of sending alerts, statistics and other data over the internet. We also needed the dashboard to be able to do a little data processing using filters and such.

### Solution
#### We chose to build a beautiful dashboard using the AdmitLTE template. The dashboard was locally hosted on the ESP32, because we put up a web server on the microcontroller which was able to host websites when u connected to it. U can now visualize and get all the above mentioned data in the dashboard.

## 3.2 Steps taken
- **Write HTML code for dashboard**
- **Create webserver on the ESP32**
- **Host dashboard on the ESP32**

## 3.3 Testing & Problems
#### We encountered no problems whatsoever with the dashboard part.

## 3.4 Proof of work
![SPRAI DASHBOARD](img/dashboard.jpeg "Sprai dashboard")

## 3.5 Files & Code
**[[Link to code](https://github.com/13A5T0N/Hackomation2020)]**

## 3.6 References & Credits
**Bits-Please**